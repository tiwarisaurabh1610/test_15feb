const mongoose=require('mongoose');
const User=require('../Model/UserModel');
const PostModel=require('../Model/PostModel');
const userValid=require('../Validation/UserValidation');
const loginValid=require('../Validation/loginValid');
const bcrypt=require('bcryptjs');
const jwt=require('jsonwebtoken');
const multer=require('multer');
const path=require('path');
const postModel = require('../Model/PostModel');

//****** Register New User *******
const insertUser = async (req, res) => {
    try {
        const dataE = await User.findOne({ email: req.body.email });
        const dataM = await User.findOne({ mobileNo: req.body.mobileNo });
        
        if (dataE !== null || dataM !== null) {
            return res.status(400).send("Already registered with given email or number");
        }

        const { error, value } = await userValid.validateAsync(req.body);
        
        if (error) {
            console.log(error.message || error);
            return res.status(400).send(error.message || error);
        }
        
        const data = await User.create(req.body);
        console.log(data); 
        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
}

//**** Create Token  ******/
const generateToken = (userId) => {
    return jwt.sign({ id: userId }, "Thisismysecretkeyitis32characterslong", { expiresIn: "2h" }); // expires in 24 hours
  }

// ******  Verify token  ****
const verifyToken=(req, res, next)=>{
    const token=req.headers['authorization'];
     if (!token) {
            return res.status(403).send({ auth: false, message: 'No token provided.' });
      }
      jwt.verify(token, "Thisismysecretkeyitis32characterslong", (err, decoded) => {
          if (err) {
               return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
              }
      
          req.userId = decoded.id;
          next();
        });
}

// ***** User Login function and jwt token created ******
const userLogin = async (req, res) => {
    try {
        console.log("Hello inside try");
        const { error, value } = await loginValid.validateAsync(req.body);
         console.log("Hello inside  in try");
        console.log(value);
        if (error) {
            console.log(error);
            return res.status(400).send(error.details.map(detail => detail.message));
        }
        const { email, password,mobileNo } = req.body;
        let user=null;
        if(email){
            user = await User.findOne({ email: email });
        }
        
        else if(mobileNo){
            user = await User.findOne({ mobileNo: mobileNo });
        }
        else {
            throw new Error("Email or mobile number is required");
        }
        if (user == null) {
            throw new Error("Oops user not found");
        }
        console.log("worlinf");
        console.log(password);
        console.log("user:",user);
        console.log(user.password);
        const isMatch = await bcrypt.compare(password, user.password);
        console.log("not matched");
        if (isMatch) {
            ////  here jwt token passed as generateToken as returned by function
            const token = generateToken(user._id);
            return res.status(200).json({ message: "Successful login", "token":token});
        }
        else {
            return res.status(401).send("Wrong password");
        }
    }
    catch (err) {
        return res.status(400).send(err.message || err);
    }
}

// ********* User Creating a post *********
const createPost=async (req, res) => {
    try {
      const post = new postModel({
        userRef: req.body.userId,
        content: req.body.content,
      });
      await post.save();
      return res.status(201).send(post);
    } 
    catch (err) {
        return res.status(500).send('Error in creating post.');
    }
  }

  // ****** User Uploading a file ********
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'Uploads/')
    },
    filename: function (req, file, cb) {
        console.log(file);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix+path.extname(file.originalname));
    }
  })
const upload = multer({ storage: storage });
const uploadFile=(req,res,next)=>{
    try{
        console.log("hello");
        console.log(req.body,req.file);
        console.log(req.body);
        const  post=PostModel.create({
            userRef: req.userId,
            content: req.body.content,
            imagePath:path.join(__dirname,"../.uploads",req.file.filename),
        })
        return res.status(200).json({"message":"File Uploaded"});
    }
    catch(err){
        console.log(err);
        return res.send(err);
    }
}

// ********* User Updating the post **********
const updatePost=async (req, res) => {
    try {
      const post = await postModel.findById(req.params.postId);
      if (!post) 
            return res.status(404).send('Post not found.');
  
      if (String(post.userRef) !== req.body.userId) {
            return res.status(403).send('You can only update your own post.');
      }
  
      post.content = req.body.content;
      await post.save();
      res.status(200).send(post);
    } catch (err) {
      res.status(500).send('Error updating post.');
    }
  }

// ******** To view all likes on a post of a user ************
const viewAllLikes=async (req, res) => {
    try {
      const post = await postModel.findById(req.params.postId).select("likes");
      if (!post) 
        return res.status(404).send('Post not found.');
        res.status(200).send(post.likes);
    }
     catch (err) {
        res.status(500).send('Error in retrieving all likes.');
    }
  };

// ********  To like a post ************
const likePost=async (req, res) => {
    try {
        const post = await PostModel.findById(req.params.postId);
        if (!post) 
            return res.status(404).send('Post not found.');
  
        if (post.likes.includes(req.body.userId)) {
            return res.status(400).send('You already liked this post.');
        }
  
        post.likes.push(req.body.userId);
        await post.save();
        return res.status(200).send(post);
    } 
     catch (err) {
        return res.status(500).send(err);
    }
  };

 //  To remove a like from a post
 const removeLike=async(req,res)=>{
    try{
        const post = await PostModel.findById(req.params.postId);
        if (!post) 
            return res.status(404).send('Post not found.');
    ////// ***** Incomplete ******
    }
    catch(err){
        res.status(500).send(err);
    }
 }


module.exports={insertUser,userLogin,upload,uploadFile,verifyToken,createPost,updatePost,viewAllLikes,likePost,removeLike};
