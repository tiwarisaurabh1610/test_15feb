const express=require('express');
const connection=require('./Connection');
require('dotenv').config();
const userRouter=require('./Routes/UserRoute');
const port = process.env.PORT || 3100;
const app=express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
connection.then(()=>{
    app.listen(port,()=>{
        console.log(`listening on port ${port}`);
    })
}).catch(()=>{
    console.log("Error in connecting with database");
}) 

app.use('/SMSite',userRouter);

app.use((req,res)=>{
    res.status(404).send("Page not found");
})