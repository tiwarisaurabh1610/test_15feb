const joi = require('joi');

 const loginSchema=joi.alternatives().try(
    joi.object({
        email: joi.string().email().required(),
        password: joi.string().min(8).required(),
    }),
    joi.object({
        dialCode:joi.string().valid('+91').required(),
        mobileNo: joi.string().required(),
        password: joi.string().min(8).required(),
    })
);
 
 
//  joi.object({
//     email: joi.string().email().required(),
//     password: joi.string().min(8).required(),
//     mobileNo:joi.string().required()
// }).or('email', 'mobileNo');

module.exports=loginSchema;
